import traceback

from xpms_helper import pandas as pd
from xpms_file_storage.file_handler import XpmsResourceFactory


def read_content(file_path, encoding="latin1"):
    try:
        # check if filepath is a urn or local path
        if file_path.startswith("minio://") or file_path.startswith("s3://"):
            file_res = XpmsResourceFactory.create_resource(urn=file_path)
            return file_res.open().data.decode("latin1")
        with open(file_path, "r", encoding=encoding) as f:
            content = f.read()
            return content
    except Exception as e:
        raise NotADirectoryError(str(e) + traceback.format_exc())


def run(datasets, config,caching=False):
    try:
        target_columns = config["func"]["configuration"]["target_column"]
        content_type = config["func"]["configuration"]["content"]
        if content_type == "text":
            extraction_func = read_content
            params = {"encoding": "latin1"}
        else:
            raise NotImplemented("content_type not supported")
        dataset = next(iter(datasets.values()))
        target_dataset = dataset["value"].loc[:, target_columns].values.tolist()
        if isinstance(target_dataset, list) and isinstance(target_dataset[0], list):
            target_dataset = target_dataset[0]
        file_content = [extraction_func(file_path, **params) for file_path in target_dataset]
        df = pd.DataFrame(columns=["content"], data=file_content)
        new_dataset = {"value": df, "data_format": "data_frame", "labels": dataset['labels']}
        return new_dataset
    except Exception as e:
        raise NotADirectoryError(str(e) + traceback.format_exc())
